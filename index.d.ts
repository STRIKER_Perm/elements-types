import Vue, {VueConstructor} from 'vue'
import { ExtendedVue } from "vue/types/vue";

export default Typo
export const Typo: TypoConstructor

export interface TypoProps {
    tag: 'p' | 'span' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6',
    textType: 'heading' | 'text',
    textVariant: string | number,
    noMargin: boolean,
}

export interface TypoComputed {
    computedClasses: () => string;
    noMarginModifier: () => string;
}

export interface TypoConstructor extends VueConstructor {
    props: TypoProps;
    computed: TypoComputed;
}

export type VueSelectInstance =
    & InstanceType<ExtendedVue<Vue, VueSelectData, VueSelectMethods, ComputedValues, VueSelectProps>>
